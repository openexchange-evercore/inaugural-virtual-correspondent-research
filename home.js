document.addEventListener('DOMContentLoaded', function() {

var pageTop = document.getElementById("page-top");
if (pageTop !== null) {

        // for link list
        var linkHeaders = document.querySelectorAll(".tile-header-ellipsis");
        // i start from 1 because one is a button for video
        for(var i = 0; i < linkHeaders.length; i ++){
            //get title first
            var linkheader = linkHeaders[i];

            if(linkheader.querySelector('.tile-title > a')){
                var tileTitle = linkheader.querySelector('.tile-title > a');
                var tileTitleText = linkheader.querySelector('.tile-title > a').text.trim();
                var newTitle = "";
                console.log(tileTitleText);
                //check if title include {date}
                if (tileTitleText.includes("{date}")) {
                    newTitle =
                    '<p class="title-date">' + tileTitleText.replace("{date}", "").trim() + '</p>';
                    // no need page description for title, so hide it
                    linkheader.querySelector('.page-description').style.display = 'none';
                    linkheader.querySelector('.tile-title').classList.add('tile-title-date');
                }else{
                    // if title is time
                    var splittedTime = tileTitleText.split("{br}"); // finde a break line, convert to array
                    splittedTime.forEach((time, index) => {
                        if (index == 0) {
                          newTitle +=
                            '<p class="title-time primary-time">' + time.trim() + "</p>";
                        } else {
                          newTitle +=
                            '<p class="title-time secondary-time">' + time.trim() + "</p>";
                        }
                    });
                    
                   
                    // if there is a time, there is a page description
                    var pageDescription = linkheader.querySelector(".page-description p.list-page-description");

                    // tileTitle.removeAttribute('target'); // on traget="_blank"
                    tileTitle.setAttribute('data-title', i); // set data attribute to make {title} clickable

                    pageDescription.innerHTML = putHtmlToAgendaPageDescription(pageDescription,i); // i is data-title
    
                }
    
                tileTitle.innerHTML = newTitle;
            }else{
                // page desription for knovio
                var pageDescription = linkheader.querySelector(".page-description p.list-page-description");
                pageDescription.innerHTML = putHtmlToKnovioPageDescription(pageDescription);
            }

            
        }
        // display list view items

        showHideElments("list-view-item", "block");

        

}

});

function showHideElments(className,option){ // block for display, none for hide
    var elements =  document.getElementsByClassName(className);
    for (var i=0;i<elements.length;i++){
        elements[i].style.display = option;
    }
}

function clickTileTitle(dataTile){
    var linkTile = document.querySelector("[data-title='"+dataTile+"']");
    linkTile.click();

}

function clickSpeaker(){
    var linkSpeaker = document.querySelector("#main_navigation_collapse .navbar-nav:nth-child(2) > li:nth-child(2) > a");
    console.log(linkSpeaker);
    linkSpeaker.click();

}

function putHtmlToAgendaPageDescription(pageDescription, dataTile){
    var pageDescriptionLists = pageDescription.textContent.split("\n");

    
    var newPageDescriptionHtml = "";
    
    pageDescriptionLists.forEach((pageDescriptionList, index) => {

        var descriptionText = pageDescriptionList.trim();

        console.log(descriptionText);

        if (descriptionText.includes("{title}")) {

            // newPageDescriptionHtml +=
            //   "<h3 class='agenda-description-title'><a onclick='clickTileTitle("+dataTile+")'>" +
            //   descriptionText.replace("{title}", "") +
            //   "</a></h3><hr>";

              newPageDescriptionHtml +=
              "<h3 class='agenda-description-title'><a onclick='clickSpeaker()'>" +
              descriptionText.replace("{title}", "") +
              "</h3></a><hr>";
              
        }else if(descriptionText.includes("{link}")) {
            newPageDescriptionHtml +=
            "<p><a class='btn-speaker-deck' onclick='clickTileTitle("+dataTile+")'>" +
            descriptionText.replace("{link}", "") +
            " <i class='fas fa-chevron-double-right' style='font-size:16px'></i></a></p>";
        }else{
            const arrayStyleClass = {
                "{bold}": "bold-text",
                "{italic}": "italic-text",
                "{color}": "color-text",
            }; 

            var styleAgendaDescription = "";

            // match style with tag {{ }}
            for (const [key, value] of Object.entries(arrayStyleClass)) {
                if (descriptionText.includes(key)) {
                styleAgendaDescription += " "+value;
                }
            }

            // append styles to p tag
            newPageDescriptionHtml +=
            "<p class='agenda-description-text " + styleAgendaDescription +"'>" + 
                descriptionText
                .replace("{bold}", "")
                .replace("{italic}", "")
                .replace("{color}", "")
             +
            "</p>";

        }
   
    });

    return newPageDescriptionHtml;
}

function putHtmlToKnovioPageDescription(pageDescription){
    var pageDescriptionLists = pageDescription.textContent.split("\n");
             
    var newPageDescriptionHtml = "";
    
    pageDescriptionLists.forEach((pageDescriptionList, index) => {

        var descriptionText = pageDescriptionList.trim();
        console.log(descriptionText);
        if (descriptionText.includes("{title}")) {
            newPageDescriptionHtml +=
              "<h3 class='knovio-page-description-title'>" +
              descriptionText.replace("{title}", "") +
              "</h3><hr>";
        }else{
            const arrayStyleClass = {
                "{bold}": "bold-text",
                "{italic}": "italic-text",
                "{color}": "color-text",
            }; 

            var styleAgendaDescription = "";

            // match style with tag {{ }}
            for (const [key, value] of Object.entries(arrayStyleClass)) {
                if (descriptionText.includes(key)) {
                styleAgendaDescription += " "+value;
                }
            }

            // append styles to p tag
            newPageDescriptionHtml +=
            "<p class='knovio-page-description-text" + styleAgendaDescription +"'>" + 
                descriptionText
                .replace("{bold}", "")
                .replace("{italic}", "")
                .replace("{color}", "")
             +
            "</p>";

        }
   
    });

    return newPageDescriptionHtml;
}