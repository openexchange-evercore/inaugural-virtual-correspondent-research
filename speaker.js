document.addEventListener('DOMContentLoaded', function() {

    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {
    
            // for link list
            var linkHeaders = document.querySelectorAll(".tile-header-ellipsis");
            // i start from 1 because one is a button for video
            for(var i = 0; i < linkHeaders.length; i ++){
                //get title first
                var linkheader = linkHeaders[i];
    
                if(linkheader.querySelector('.tile-title > a')){
                    var tileTitle = linkheader.querySelector('.tile-title > a');
       
                    var pageDescription = linkheader.querySelector(".page-description p.list-page-description");

                    tileTitle.setAttribute('data-title', i); // set data attribute to make {title} clickable

                    pageDescription.innerHTML = putHtmlToAgendaPageDescription(pageDescription,i); // i is data-title
        
                }
    
            }
            // display list view items
    
            showHideElments("list-view-item", "block");
    
            
    
    }
    
    });
    
    function showHideElments(className,option){ // block for display, none for hide
        var elements =  document.getElementsByClassName(className);
        for (var i=0;i<elements.length;i++){
            elements[i].style.display = option;
        }
    }
    
    function clickTileTitle(dataTile){
        var linkTile = document.querySelector("[data-title='"+dataTile+"']");
        linkTile.click();
    
    }
    
    function putHtmlToAgendaPageDescription(pageDescription, dataTile){
        var pageDescriptionLists = pageDescription.textContent.split("\n");
    
        
        var newPageDescriptionHtml = "";
        
        pageDescriptionLists.forEach((pageDescriptionList, index) => {
    
            var descriptionText = pageDescriptionList.trim();
    
            console.log(descriptionText);
    
            if(descriptionText.includes("{link}")) {
                newPageDescriptionHtml +=
                "<p><a class='btn-speaker-deck' onclick='clickTileTitle("+dataTile+")'>" +
                descriptionText.replace("{link}", "") +
                " <i class='fas fa-chevron-double-right' style='font-size:16px'></i></a></p>";
            }else{
                const arrayStyleClass = {
                    "{bold}": "bold-text",
                    "{italic}": "italic-text",
                    "{color}": "color-text",
                }; 
    
                var styleAgendaDescription = "";
    
                // match style with tag {{ }}
                for (const [key, value] of Object.entries(arrayStyleClass)) {
                    if (descriptionText.includes(key)) {
                    styleAgendaDescription += " "+value;
                    }
                }
    
                // append styles to p tag
                newPageDescriptionHtml +=
                "<p class='speaker-description-text " + styleAgendaDescription +"'>" + 
                    descriptionText
                    .replace("{bold}", "")
                    .replace("{italic}", "")
                    .replace("{color}", "")
                 +
                "</p>";
    
            }
       
        });
    
        return newPageDescriptionHtml;
    }
